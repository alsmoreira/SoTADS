#!/bin/bash

j=42

function so(){
         for((i=1;i<=10;i++));
         do
           echo -ne "\e[${1}m \e[0m $((i * 5))% \b\b\b\b\b"
           sleep .03
         done

         echo "  "
		 #conversao de CRLF para LF
		 #tr -d '\r' < Makefile.1 > Makefile 
         make 2>./log.txt

         for((i=11;i<=19;i++));
         do
           echo -ne "\e[${1}m \e[0m $((i * 5))% \b\b\b\b\b"
           sleep .03
         done
         echo "  "
         mkdir -p .fs
         mv *.o ./.fs/
         mv _* ./.fs/
         mv *.d ./.fs/
         mv *.asm ./.fs/
         mv *.sym ./.fs/
         mv *.out ./.fs/
         mv *.S ./.fs/
         mv *.img ./.fs/
         mv bootblock ./.fs/
         mv entryother ./.fs/
         mv initcode ./.fs/
         mv mkfs ./.fs/
         mv kernel ./.fs/

         echo $((i * 5))"%"

         #não tirar a linha

         qemu-system-x86_64 -nographic -drive file=.fs/fs.img,index=1,media=disk,format=raw -drive file=.fs/xv6.img,index=0,media=disk,format=raw -smp 2 -m 512 2>../../log.txt
}

echo "Compilando Sistema Operacional: "
so $j
