# Introdução

SoTADS é um  _fork_  do sistema operacional educacional xv6. Sendo que este é uma
reimplementação do Unix de Dennis Ritchie e Ken Thompson. Porém é implmentado em uma
plataforma multiprocessador do x86 da Intel usando a linguagem ANSI C.

## Considerações

xv6 é inspirado em comentários de John Lions sobre a 6ª edição de UNIX (Comunicação Ponto-a-Ponto; ISBN: 1-57398-013-7; (14 de Junho de 2000)). Mais informações em [PDOS](http://pdos.csail.mit.edu/6.828/2007/v6.html), which
tem informações sobre os recursos on-line do v6.

xv6 Tem códigos emprestados de:
* JOS (asm.h, elf.h, mmu.h, bootasm.S, ide.c, console.c, e outros);
* Plan 9 (entryother.S, mp.h, mp.c, lapic.c);
* FreeBSD (ioapic.c);
* NetBSD (console.c).

Contribuidores:
* Russ Cox (troca de contexto, bloqueio);
* Cliff Frey (MP);
* Xiao Yu (MP);
* Nickolai Zeldovich;
* Austin Clements.

Para executar precisa do QEMU.
Um script pode ser utilizado diretamente, executando em um terminal:

<div class="highlight highlight-source-shell"><pre>start.sh</pre></div>

