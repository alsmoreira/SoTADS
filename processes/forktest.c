// Test that fork fails gracefully.
// Tiny executable so that the limit can be filling the proc table.

#include "../headers/types.h"
#include "../fs/stat.h"
#include "../ul/user.h"

#define N  1000

void
printf(int fd, const char *s, ...)
{
  write(fd, s, strlen(s));
}

void
forktest(void)
{
  int n, pid;

  printf(1, "fork teste\n");

  for(n=0; n<N; n++){
    pid = fork();
    if(pid < 0)
      break;
    if(pid == 0)
      exit();
  }

  if(n == N){
    printf(1, "fork convocado para trabalhar N vezes!\n", N);
    exit();
  }

  for(; n > 0; n--){
    if(wait() < 0){
      printf(1, "wait parou cedo\n");
      exit();
    }
  }

  if(wait() != -1){
    printf(1, "wait muitas vezes\n");
    exit();
  }

  printf(1, "fork teste OK\n");
}

int
main(void)
{
  forktest();
  exit();
}
