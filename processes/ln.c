#include "../headers/types.h"
#include "../fs/stat.h"
#include "../ul/user.h"

int
main(int argc, char *argv[])
{
  if(argc != 3){
    printf(2, "Usage: ln antigo novo\n");
    exit();
  }
  if(link(argv[1], argv[2]) < 0)
    printf(2, "link %s %s: falhou\n", argv[1], argv[2]);
  exit();
}
